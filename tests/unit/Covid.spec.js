import { shallowMount } from '@vue/test-utils';
import Covid from '@/components/Covid.vue';

describe('Covid', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Covid, {

    });
  });

  it('loadPage', () => {
    const title = wrapper.find('h2');
    expect(title.text()).toEqual('Covid 19 Stats');
  });
});